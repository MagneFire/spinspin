package org.magnefire.spinspin.Providers;

import android.content.Context;

public class egz extends DynamicProvider {
    public egz(Context context) {
        super(context);
        mRequestType = RequestType.OBJECT;
        mProviderQuality = 8;
        mProviderUrl = "http://easygamezone.com/apps/cmapi/api/v2.php/v2.php?&getFromApp=Yes&package_name=com.easygamezone.cmlinks";
        mParserFieldArray = "data";
        mParserFieldText = "title";
        mParserFieldUrl = "url";
        mParserFieldDate = "create";
        mDateFormat = new String[1];
        mDateFormat[0] = "dd-MM-yyyy hh:mm:ss a";
    }
}
