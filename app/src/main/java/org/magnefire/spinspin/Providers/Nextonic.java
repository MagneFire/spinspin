package org.magnefire.spinspin.Providers;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.magnefire.spinspin.Provider;
import org.magnefire.spinspin.Reward;
import org.magnefire.spinspin.RewardParser;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Nextonic extends Provider {
    /// Set the data quality that the provider provides on its own.
    public static final int PROVIDER_QUALITY = -10;
    String token;
    Reward.RewardType itemType;

    String tokenKey(String str) {
        try {
            str = new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes())).toString(16);
            while (str.length() < 32) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("0");
                stringBuilder.append(str);
                str = stringBuilder.toString();
            }
            return str;
        } catch (NoSuchAlgorithmException e) {
            Log.e("MD5", e.getLocalizedMessage());
            return null;
        }
    }

    Nextonic(Context context) {
        super(context);
        token = tokenKey("com.moon.spins.coins");
    }

    private Date getDate(String dateString) {
        Date date = new Date(0);
        try {
            date = new SimpleDateFormat("dd-MMM-yyyy, hh:mm:ss a", Locale.US).parse(dateString);
        } catch (Exception e) {
            // Ignore error.
        }
        return date;
    }

    protected void handleData(JSONArray response) {
        try {
            rewards.clear();
            for (int i = 0; i < response.length(); i++) {
                String text = response.getJSONObject(i).getString("cm_title");
                String url = response.getJSONObject(i).getString("cm_url").replace("\\", "");
                // Skip reward if blacklisted.
                if (RewardParser.isBlacklisted(text, url)) continue;

                String date = response.getJSONObject(i).getString("cm_date") + ", " + response.getJSONObject(i).getString("cm_time");
                Reward reward = new Reward();
                reward.itemType = itemType;
                reward.url = url;
                reward.date = getDate(date);
                RewardParser.parseString(reward, text);
                reward.predictionQuality += PROVIDER_QUALITY;
                rewards.add(reward);
            }
            // Important, mark as fetched after adding the rewards.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataReady(this);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }

    public void requestList() {
    }

}
