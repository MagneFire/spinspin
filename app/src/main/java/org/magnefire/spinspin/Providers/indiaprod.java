package org.magnefire.spinspin.Providers;

import android.content.Context;

public class indiaprod extends DynamicProvider {
    public indiaprod(Context context) {
        super(context);
        mRequestType = RequestType.OBJECT;
        mProviderQuality = 8;
        mProviderUrl = "http://indiaprod.in/api/get_coins.php";
        mParserFieldArray = "data";
        mParserFieldText = "posttitle";
        mParserFieldUrl = "link";
        mParserFieldDate = "created";
        mDateFormat = new String[1];
        mDateFormat[0] = "yyyy-MM-dd HH:mm:ss";
    }
}
