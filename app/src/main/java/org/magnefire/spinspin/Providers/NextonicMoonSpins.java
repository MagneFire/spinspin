package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.magnefire.spinspin.Reward;

public class NextonicMoonSpins extends Nextonic {
    private static final String ROOT_URL = "https://nextonicdeveloper.com/api/moon/urls/spins/";

    public NextonicMoonSpins(Context context) {
        super(context);
        itemType = Reward.RewardType.REWARD_TYPE_SPINS;
    }

    public void requestList() {
        isDone = false;
        getStandardJsonArray(ROOT_URL + token);
    }

}
