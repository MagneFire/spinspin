package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;
import org.magnefire.spinspin.Provider;
import org.magnefire.spinspin.Reward;
import org.magnefire.spinspin.RewardParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Smpartner extends Provider {
    /// Set the data quality that the provider provides on its own.
    public static final int PROVIDER_QUALITY = 10;
    private static final String SMPARTNER_URL = "https://videoapp.smpartner.in/api.php?cat_id=76";


    public Smpartner(Context context) {
        super(context);
    }

    private Date getDate(String dateString) {
        Date date = new Date(0);
        // Special patch for this dataset.
        dateString = dateString.replace("219", "2019");
        try {
            date = new SimpleDateFormat("dd MMM yyyy, hh:mm a", Locale.US).parse(dateString);
        } catch (Exception e) {
            try {
                date = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US).parse(dateString);
            } catch (Exception e2) {
                try {
                    date = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US).parse(dateString);
                } catch (Exception e3) {
                    // Ignore error.
                }
            }
        }
        return date;
    }

    protected void handleData(JSONObject response) {
        try {
            JSONArray a = response.getJSONArray("ALL_IN_ONE_VIDEO");
            rewards.clear();
            for (int i = 0; i < a.length(); i++) {
                String text = a.getJSONObject(i).getString("video_title");
                String url = (a.getJSONObject(i).getString("video_url"));
                // Skip reward if blacklisted.
                if (RewardParser.isBlacklisted(text, url)) continue;

                String date = a.getJSONObject(i).getString("video_duration");
                Reward reward = new Reward();
                reward.url = url;
                reward.date = getDate(date);
                RewardParser.parseString(reward, text);
                reward.predictionQuality += PROVIDER_QUALITY;
                rewards.add(reward);
            }
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataReady(this);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }

    public void requestList() {
        isDone = false;
        getStandardJsonObject(SMPARTNER_URL);
    }
}
