package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.magnefire.spinspin.Reward;

public class NextonicEcoCoins extends Nextonic {
    private static final String ROOT_URL = "https://nextonicdeveloper.com/api/ecoveta/urls/coins/";

    public NextonicEcoCoins(Context context) {
        super(context);
        token = tokenKey("com.eco.spins.coins");
        itemType = Reward.RewardType.REWARD_TYPE_COINS;
    }

    public void requestList() {
        isDone = false;
        getStandardJsonArray(ROOT_URL + token);
    }

}
