package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;
import org.magnefire.spinspin.Provider;
import org.magnefire.spinspin.Reward;
import org.magnefire.spinspin.RewardParser;

import java.util.Date;

public class Haktuts extends Provider {
    /// Set the data quality that the provider provides on its own.
    public static final int PROVIDER_QUALITY = 8;
    private static final String PROVIDER_URL = "http://pktechnolabs.in/cmadvance/api/v1/getlinks.php";

    public Haktuts(Context context) {
        super(context);
    }

    protected void handleData(JSONObject response) {
        try {
            JSONArray a = response.getJSONArray("data");
            rewards.clear();
            for (int i = 0; i < a.length(); i++) {
                String text = (a.getJSONObject(i).getString("description"));
                String url = a.getJSONObject(i).getString("link").replace("\\", "");
                // Skip reward if blacklisted.
                if (RewardParser.isBlacklisted(text, url)) continue;

                long timestamp = Integer.parseInt(a.getJSONObject(i).getString("created_timestamp"));
                // Convert to milliseconds.
                timestamp = timestamp * 1000;
                String categoryName = a.getJSONObject(i).getString("category_name");
                Reward reward = new Reward();
                if (categoryName.toLowerCase().contains("spin")) {
                    reward.itemType = Reward.RewardType.REWARD_TYPE_SPINS;
                } else if (categoryName.toLowerCase().contains("coin")) {
                    reward.itemType = Reward.RewardType.REWARD_TYPE_COINS;
                }
                reward.url = url;
                reward.date = new Date(timestamp);
                RewardParser.parseString(reward, text);
                reward.predictionQuality += PROVIDER_QUALITY;
                rewards.add(reward);
            }
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataReady(this);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }

    public void requestList() {
        isDone = false;
        getStandardJsonObject(PROVIDER_URL);
    }
}
