package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;
import org.magnefire.spinspin.Provider;
import org.magnefire.spinspin.Reward;
import org.magnefire.spinspin.RewardParser;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppsExtent extends Provider {
    /// Set the data quality that the provider provides on its own.
    public static final int PROVIDER_QUALITY = -10;
    private static final String DEVLO_URL = "http://appsexteandroidvideouser.appsextent.com/getVideoDataAppsextent.php?name=spin%20and%20coin&bundle=dailyfree.spincoin.moonlab.links";

    public AppsExtent(Context context) {
        super(context);
    }
    private Date getDate(String dateString) {
        Date date = new Date(0);
        try {
            date = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US).parse(dateString);
        } catch (Exception e) {
            // Ignore error.
        }
        return date;
    }

    private String parseUrl(String encoded) {
        String url = "";
        try {
            url = java.net.URLDecoder.decode(encoded, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            // not going to happen - value came from JDK's own StandardCharsets
        }
        // Extract App link from facebook url if url is a facebook url.
        if (url.contains("facebook") && url.contains("u=")) {
            url = url.substring(url.indexOf("u=")+2);
        }
        return url;
    }

    protected void handleData(JSONObject response) {
        try {
            JSONArray a = response.getJSONArray("data");
            rewards.clear();
            for (int i = 0; i < a.length(); i++) {
                String text = (a.getJSONObject(i).getString("description"));
                String url = parseUrl(a.getJSONObject(i).getString("url"));
                // Skip reward if blacklisted.
                if (RewardParser.isBlacklisted(text, url)) continue;
                // Links containing the this text only open the app in the Play Store.
                if (url.contains("CoinMasterGame")) continue;

                String date = a.getJSONObject(i).getString("updatedAt");
                Reward reward = new Reward();
                reward.url = url;
                reward.date = getDate(date);
                RewardParser.parseString(reward, text);
                reward.predictionQuality += PROVIDER_QUALITY;
                rewards.add(reward);
            }
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataReady(this);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }

    public void requestList() {
        isDone = false;
        getStandardJsonObject(DEVLO_URL);
    }
}
