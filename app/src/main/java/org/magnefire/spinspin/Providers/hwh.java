package org.magnefire.spinspin.Providers;

import android.content.Context;

public class hwh extends DynamicProvider {
    public hwh(Context context) {
        super(context);
        mRequestType = RequestType.OBJECT;
        mProviderQuality = 6;
        mProviderUrl = "http://api.hashtagwebhub.com/web_service/get_service.php?service_type=get_rewards&app_id=113";
        mParserFieldArray = "link_details";
        mParserFieldText = "title";
        mParserFieldUrl = "link";
        mParserFieldDate = "post_date";
        mDateFormat = new String[1];
        mDateFormat[0] = "MMM dd, yyyy, hh:mm a";
    }
}
