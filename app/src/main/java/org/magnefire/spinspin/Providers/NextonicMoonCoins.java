package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.magnefire.spinspin.Reward;

public class NextonicMoonCoins extends Nextonic {
    private static final String ROOT_URL = "https://nextonicdeveloper.com/api/moon/urls/coins/";

    public NextonicMoonCoins(Context context) {
        super(context);
        itemType = Reward.RewardType.REWARD_TYPE_COINS;
    }

    public void requestList() {
        isDone = false;
        getStandardJsonArray(ROOT_URL + token);
    }

}
