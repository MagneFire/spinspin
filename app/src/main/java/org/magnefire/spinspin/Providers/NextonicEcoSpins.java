package org.magnefire.spinspin.Providers;

import android.content.Context;

import org.magnefire.spinspin.Reward;

public class NextonicEcoSpins extends Nextonic {
    private static final String ROOT_URL = "https://nextonicdeveloper.com/api/ecoveta/urls/spins/";

    public NextonicEcoSpins(Context context) {
        super(context);
        token = tokenKey("com.eco.spins.coins");
        itemType = Reward.RewardType.REWARD_TYPE_SPINS;
    }

    public void requestList() {
        isDone = false;
        getStandardJsonArray(ROOT_URL + token);
    }

}
