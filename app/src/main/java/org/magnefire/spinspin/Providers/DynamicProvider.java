package org.magnefire.spinspin.Providers;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.magnefire.spinspin.Provider;
import org.magnefire.spinspin.Reward;
import org.magnefire.spinspin.RewardParser;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DynamicProvider extends Provider {
    /// Set the data quality that the provider provides on its own.
    //public static final int PROVIDER_QUALITY = 8;
    //private static final String PROVIDER_URL = "http://indiaprod.in/api/get_coins.php";
    public interface DynamicProviderData {
        void requestList();
        void handleData(JSONArray a);
        void handleData(JSONObject response);
        void handleData(String response);
    }
    public enum RequestType {
        UNKNOWN,
        ARRAY,
        OBJECT,
        STRING
    };
    public RequestType mRequestType = RequestType.UNKNOWN;
    public int mProviderQuality = 0;
    public int id = -1;
    public String mProviderUrl = "";
    public String mDateFormat[] = null;

    /// Array field name.
    public String mParserFieldArray = "rewards";
    /// Field that contains information about what the reward is.
    public String mParserFieldText = "text";
    /// Field that contains the link to the reward.
    public String mParserFieldUrl = "url";
    /// Field that contains the date of the reward.
    public String mParserFieldDate = "date";

    public DynamicProviderData mScriptHandler = null;

    public DynamicProvider(Context context) {
        super(context);
    }
    private Date getDate(String dateString) {
        if (mDateFormat == null) return null;
        Date date = new Date(0);
        for (String format : mDateFormat) {
            try {
                date = new SimpleDateFormat(format, Locale.US).parse(dateString);
                // Date parsed successfully, stop here.
                break;
            } catch (Exception e) {
                // Ignore error.
            }
        }
        return date;
    }

    protected void parseData(JSONArray a) {
        try {
            rewards.clear();
            for (int i = 0; i < a.length(); i++) {
                String text = (a.getJSONObject(i).getString(mParserFieldText));
                String url = a.getJSONObject(i).getString(mParserFieldUrl);
                try {
                    url = URLDecoder.decode(url, "UTF-8");
                } catch (Exception e) {
                    // Ignore
                }
                // Skip reward if blacklisted.
                if (RewardParser.isBlacklisted(text, url)) continue;

                String date = a.getJSONObject(i).getString(mParserFieldDate);
                Reward reward = new Reward();
                reward.url = url;
                reward.date = getDate(date);
                RewardParser.parseString(reward, text);
                reward.predictionQuality += mProviderQuality;
                rewards.add(reward);
            }
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataReady(this);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }
    public void parseData(JSONObject response) {
        try {
            JSONArray a = response.getJSONArray(mParserFieldArray);
            // Handle data :)
            parseData(a);
        } catch (Throwable e) {
            e.printStackTrace();
            // Mark data as fetched.
            isDone = true;
            // Call callback.
            if (callback != null)
                callback.onProviderDataError(this, null);
        }
    }

    public void handleData(JSONObject response) {
        // If a script handler is defined, we assume that this provider is used in conjunction with the API.
        // Otherwise forward call to parse the response as valid reward data.
        if (mScriptHandler != null)
            mScriptHandler.handleData(response);
        else
            parseData(response);
    }
    public void handleData(JSONArray response) {
        // If a script handler is defined, we assume that this provider is used in conjunction with the API.
        // Otherwise forward call to parse the response as valid reward data.
        if (mScriptHandler != null)
            mScriptHandler.handleData(response);
        else
            parseData(response);
    }
    public void handleData(String response) {
        if (mScriptHandler == null) return;
        mScriptHandler.handleData(response);
    }

    public void requestList() {
        isDone = false;
        if (mProviderUrl == null) {
            if (mScriptHandler == null) return;
            mScriptHandler.requestList();
            return;
        }
        switch (mRequestType) {
            case OBJECT:
                getStandardJsonObject(mProviderUrl);
                break;
            case ARRAY:
                getStandardJsonArray(mProviderUrl);
                break;
            case STRING:
                getString(mProviderUrl);
                break;
            default:
                isDone = true;
                Log.e(getClass().getName(), "No valid request type was defined!");
                break;
        }
    }
}
