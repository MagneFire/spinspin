package org.magnefire.spinspin.Providers;

import android.content.Context;

public class cst extends DynamicProvider {
    public cst(Context context) {
        super(context);
        mRequestType = RequestType.OBJECT;
        mProviderQuality = 8;
        mProviderUrl = "http://cyberspacetechnologies.net/sandy/coinApi/api/v2.php";
        mParserFieldArray = "data";
        mParserFieldText = "title";
        mParserFieldUrl = "url";
        mParserFieldDate = "create";
        mDateFormat = new String[1];
        mDateFormat[0] = "dd-MM-yyyy hh:mm:ss a";
    }
}
