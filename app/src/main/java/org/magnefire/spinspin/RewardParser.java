package org.magnefire.spinspin;

import java.util.Arrays;

public class RewardParser {

    public static void parseString(Reward reward, String text) {
        int digits;
        if (text == null) return;
        text = text.toLowerCase();
        int offsets[] = {text.indexOf("spin"), text.indexOf("coin")};

        // Initialize numbers to 0.
        int number[] = new int[2];
        int multiplier[] = new int[2];
        // Set default multiplier to 1.
        Arrays.fill(multiplier, 1);
        // Find numbers and multipliers in string.
        for (int i = 0; i < offsets.length; i++) {
            digits = 0;
            if (offsets[i] != -1) {
                for (int str_offset = offsets[i]; str_offset >= 0; str_offset--) {
                    if (text.charAt(str_offset) >= '0' && text.charAt(str_offset) <= '9') {
                        // Multiply digit times the 10^(amount of digits). This puts digits in front
                        // of the previously assigned number.
                        number[i] += ((text.charAt(str_offset) - 48) * Math.pow(10, digits));
                        digits += 1;
                    } else if (text.charAt(str_offset) == 'k') {
                        multiplier[i] = 1000;
                    } else if (text.charAt(str_offset) == 'm') {
                        multiplier[i] = 1000000;
                    } else if (text.charAt(str_offset) == 'b') {
                        multiplier[i] = 1000000000;
                    } else if (digits > 0) {
                        if ((text.charAt(str_offset) == '.') || (text.charAt(str_offset) == ',')) {
                            multiplier[i] *= Math.pow(10, -digits);
                        } else {
                            // If we parsed a value and the current character is not a valid number => break.
                            // Ignore current character if no number was parsed yet.
                            break;
                        }
                    }
                }
            }
        }
        if (offsets[0] != -1 || reward.itemType == Reward.RewardType.REWARD_TYPE_SPINS) {
            reward.itemType = Reward.RewardType.REWARD_TYPE_SPINS;
            if (number[0] > 0) {
                reward.predictionQuality = Reward.PredictionQuality.GOOD.getValue();
                reward.nrOfSpins = number[0];
            } else {
                reward.predictionQuality = Reward.PredictionQuality.SUFFICIENT.getValue();
                // Generally we can assume that a spins only reward is 25.
                reward.nrOfSpins = 25;
            }
        }
        if (offsets[1] != -1 || reward.itemType == Reward.RewardType.REWARD_TYPE_COINS) {
            reward.itemType = Reward.RewardType.REWARD_TYPE_COINS;
            reward.multiplierCoins = multiplier[1];
            if (number[1] > 0) {
                reward.predictionQuality = Reward.PredictionQuality.GOOD.getValue();
                reward.nrOfCoins = number[1];
            } else {
                reward.predictionQuality = Reward.PredictionQuality.SUFFICIENT.getValue();
                // Generally we can assume that a coins only reward is 20M.
                reward.nrOfCoins = 20;
                reward.multiplierCoins = 1000000;
            }
        }
        if ((offsets[0] != -1) && (offsets[1] != -1) || reward.itemType == Reward.RewardType.REWARD_TYPE_SPINS_AND_COINS) {
            reward.itemType = Reward.RewardType.REWARD_TYPE_SPINS_AND_COINS;
            // If reward wasn't parsed properly.
            if (number[1] == 0 && number[0] == 0) {
                reward.predictionQuality = Reward.PredictionQuality.SUFFICIENT.getValue();
                // Generally we can assume that spins and coins are 10 and 1M.
                reward.nrOfCoins = 1;
                reward.multiplierCoins = 1000000;
                reward.nrOfSpins = 10;
            }
        }
    }

    public static boolean isBlacklisted(String text, String url) {
        if (url.isEmpty()) {
            // Empty urls should never be added.
            return true;
        } else if (text.contains("download") || text.contains("review") || text.contains("install")) {
            // Description contains words that make no sense for an spin/coin text. Blacklist reward.
            return true;
        } else if (url.contains("play.google.com")) {
            // URL list link to another app, black list reward.
            return true;
        }
        return false;
    }
}
