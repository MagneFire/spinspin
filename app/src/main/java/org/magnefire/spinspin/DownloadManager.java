// The code written here is based from https://stackoverflow.com/questions/15758856/android-how-to-download-file-from-webserver

package org.magnefire.spinspin;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class DownloadManager extends AsyncTask<DownloadManager.File, Integer, DownloadManager.File> {

    private Complete mCallback;

    public void setCallback(Complete callback) {
        mCallback = callback;
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected DownloadManager.File doInBackground(DownloadManager.File... files) {
        int count;
        try {
            URL url = new URL(files[0].downloadUrl);
            URLConnection conection = url.openConnection();
            conection.connect();

            int lenghtOfFile = conection.getContentLength();

            // Download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream
            OutputStream output = files[0].outputFile;

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // Publish progress if length is known.
                if (lenghtOfFile > 0)
                    publishProgress((int) (total * 100 / lenghtOfFile));

                // Writing data to file
                output.write(data, 0, count);
            }

            // Flush output
            output.flush();

            // Close all.
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            files[0].state = 1;
            return files[0];
        }

        return files[0];
    }

    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // Print progress.
        Log.i(getClass().getName(), "Progress: "+progress[0]);
    }

    @Override
    protected void onPostExecute(File result) {
        if (result.state != 0) {
            mCallback.OnDownloadComplete(result, false);
        } else {
            mCallback.OnDownloadComplete(result, true);
        }
    }

    static class File {
        String downloadUrl;
        OutputStream outputFile;
        int state;
        public File(String f,OutputStream s) {
            downloadUrl = f;
            outputFile = s;
        }
    }
    public interface Complete {
        void OnDownloadComplete(File file, boolean success);
    }
}