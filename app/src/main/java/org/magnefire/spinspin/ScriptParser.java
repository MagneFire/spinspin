package org.magnefire.spinspin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.magnefire.spinspin.Providers.DynamicProvider;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ScriptParser {
    // Scripting environment :)
    private WebView mWebView;

    private Context mContext;
    private Activity mActivity;

    // Used to keep track of what providers are used for use within the API(JavaScript) code.
    private ArrayList<DynamicProvider> mDynamicProviders;

    // Manager all providers, used to add new providers.
    private ProviderManager mProviderManager;

    private ScriptReady mCallback;

    public void setCallback(ScriptReady callback) {
        mCallback = callback;
    }

    public interface ScriptReady {
        void onJsLoaded();
    }

    public ScriptParser(Context context, Activity activity) {
        mContext = context;
        mActivity = activity;
        mProviderManager = ProviderManager.getInstance();

        mWebView = new WebView(mContext);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // Enable storage from js.
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        // Allow cross origin.
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        // Add ChromeClient for debugging, also allows for JavaScript popups to appear.
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                // Do not block request.
                result.confirm();
                AlertDialog dialog = new AlertDialog.Builder(view.getContext()).
                        setTitle(mContext.getString(R.string.app_name)).
                        setMessage(message).
                        setPositiveButton(mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        }).create();
                dialog.show();
                return true;
            }
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.e(getClass().getName(), consoleMessage.message());
                return true;
            }
        });
        mWebView.addJavascriptInterface(this, "Api");
        mDynamicProviders = new ArrayList<>();
    }

    public void parse(String script) {
        if (script == null) return;
        mWebView.loadDataWithBaseURL("file:///android_asset/www/index.html", script, "text/html", "UTF-8", "");
    }

    public void parseFromFile(String url) {
        String scriptData = null;
        try {
            FileInputStream in = mContext.openFileInput(url);
            InputStreamReader inputStreamReader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String line;
            sb.append("<script>");
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            sb.append("</script>");
            scriptData = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (scriptData != null) {
            parse(scriptData);
        } else
            // No script loaded, continue fetching data from providers.
            jsLoaded();
    }

    private void getStandardJson(DynamicProvider.RequestType type, final int id, int quality, String url, String format[]) {
        // Perform a standard request for Json object data/Json array/String.
        // Result should be parsed using javascript i.e. call to webview.
        DynamicProvider dp = new DynamicProvider(mContext);
        dp.id = id;
        dp.mProviderQuality = quality;
        dp.mProviderUrl = url;
        dp.mDateFormat = format;
        dp.mRequestType = type;
        // Let JavaScript handle the result. (i.e. parse data to something the dynamic provider understands)
        dp.mScriptHandler = new DynamicProvider.DynamicProviderData() {
            @Override
            public void requestList() {
                mWebView.loadUrl("javascript: request"+id+"()");
            }
            @Override
            public void handleData(JSONObject response) {
                mWebView.loadUrl("javascript: parse"+id+"("+response+")");
            }
            @Override
            public void handleData(JSONArray response) {
                mWebView.loadUrl("javascript: parse"+id+"("+response+")");
            }
            @Override
            public void handleData(String response) {
                mWebView.loadUrl("javascript: parse"+id+"(\""+response+"\")");
            }
        };
        mDynamicProviders.add(id, dp);
        mProviderManager.addProvider(dp);
        // It is possible that dynamic providers are added after the fetchLists() call from the onCreate() function.
        // Make sure that this data is also fetched, just in case.
        dp.requestList();
    }

    /**********************************************************************************************
    ********************************* SPINSPIN API FUNCTION CALLS *********************************
    **********************************************************************************************/

    @JavascriptInterface
    public void jsLoaded() {
        // Provide a function for the scipt to call to indicate that the script has properly loaded.
        // Used to indicate that the main thread can continue fetching the the lists.
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallback.onJsLoaded();
            }
        });
    }

    @JavascriptInterface
    public void parseRewards(int id, String data) {
        // This function assumes that the data is a list of rewards.
        final DynamicProvider dp = mDynamicProviders.get(id);
        try {
            final JSONObject obj = new JSONObject(data);
            // Extract rewards in standard format.
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dp.parseData(obj);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void createProvider(final int id, final int quality, final String[] format) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getStandardJson(DynamicProvider.RequestType.OBJECT, id, quality, null, format);
            }
        });
    }

    @JavascriptInterface
    public void disableBuiltin(final String name) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Disable built-in provider.
                mProviderManager.removeProvider(name);
            }
        });
    }

    @JavascriptInterface
    public void requestJsonObject(final int id, final int quality, final String url, final String[] format) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getStandardJson(DynamicProvider.RequestType.OBJECT, id, quality, url, format);
            }
        });
    }

    @JavascriptInterface
    public void requestJsonArray(final int id, final int quality, final String url, final String[] format) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getStandardJson(DynamicProvider.RequestType.ARRAY, id, quality, url, format);
            }
        });
    }

    @JavascriptInterface
    public void requestString(final int id, final int quality, final String url, final String[] format) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getStandardJson(DynamicProvider.RequestType.STRING, id, quality, url, format);
            }
        });
    }
}
