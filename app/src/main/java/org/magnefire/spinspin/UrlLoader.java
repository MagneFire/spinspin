package org.magnefire.spinspin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.net.URISyntaxException;

public class UrlLoader extends AppCompatActivity {
    private Context context;
    private int position_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url_loader);
        // Add back button to toolbar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();

        context = this;

        String message = intent.getStringExtra(MainActivity.URL_MESSAGE);
        position_message = intent.getIntExtra(MainActivity.POSITION_MESSAGE, -1);
        WebView webView = findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null) return false;
                setTitle("Loading " + url);

                if (url.startsWith("http://") || url.startsWith("https://")) return false;

                if (url.startsWith("intent://")) {
                    try {
                        Context context = view.getContext();
                        Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                        if (intent != null) {
                            view.stopLoading();

                            PackageManager packageManager = context.getPackageManager();
                            ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
                            if (info != null) {
                                context.startActivity(intent);
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra(MainActivity.POSITION_MESSAGE, position_message);
                                setResult(Activity.RESULT_OK,returnIntent);
                                // Go back.
                                finish();
                            } else {
                                String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                                view.loadUrl(fallbackUrl);

                                // or call external broswer
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fallbackUrl));
//                    context.startActivity(browserIntent);
                            }

                            return true;
                        }
                    } catch (URISyntaxException e) {
                        Toast.makeText(context, getString(R.string.failed_to_find_app), Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        view.getContext().startActivity(intent);
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(MainActivity.POSITION_MESSAGE, position_message);
                        setResult(Activity.RESULT_OK,returnIntent);
                        // Go back.
                        finish();
                        return true;
                    } catch (Exception e) {
                        Toast.makeText(context, getString(R.string.failed_to_find_app), Toast.LENGTH_LONG).show();
                        return true;
                    }
                }
                return true;
            }

        });
        setTitle("Loading " + message);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(message);
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(MainActivity.POSITION_MESSAGE, position_message);
        setResult(Activity.RESULT_CANCELED,returnIntent);
        // Go back.
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(MainActivity.POSITION_MESSAGE, position_message);
            setResult(Activity.RESULT_CANCELED,returnIntent);
            // Go back.
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
