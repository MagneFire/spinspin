package org.magnefire.spinspin;

import java.util.Date;

public class Reward {

    public enum RewardType{
        REWARD_TYPE_UNKNOWN,
        REWARD_TYPE_SPINS,
        REWARD_TYPE_COINS,
        REWARD_TYPE_SPINS_AND_COINS
    }

    public enum PredictionQuality{
        NO_PREDICTION(0),
        BAD(1),
        SUFFICIENT(50),
        GOOD(100),
        ACCUATE(200);

        private final int q;
        PredictionQuality(int q){this.q = q;}
        public int getValue() {return q;}
    }

    public int predictionQuality = PredictionQuality.NO_PREDICTION.getValue();
    public RewardType itemType = RewardType.REWARD_TYPE_UNKNOWN;
    public int nrOfSpins = 0;
    public int nrOfCoins = 0;
    public int multiplierCoins = 1;

    public String url = null;
    public Date date = null;
    public boolean used = false;

    public Reward() {}
    public Reward(String url) {
        this.url = url;
    }
}

