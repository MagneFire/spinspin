package org.magnefire.spinspin;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Provider {

    public String mProviderName = null;

    static final String PROVIDER_ITEM_USED = "item_used_";

    private Context context;
    protected List<Reward> rewards;

    protected OnProviderReady callback;

    protected boolean isDone = true;

    protected Provider(Context context) {
        mProviderName = getClass().getName();
        this.context = context;
        rewards = new ArrayList<>();
    }

    void setOnProviderReady(OnProviderReady callback) {
        if (this.callback != null) this.callback.onProviderRemoved(this);
        this.callback = callback;
    }
    public void requestList() {}

    public interface OnProviderReady {
        void onProviderDataReady(Provider provider);
        void onProviderDataError(Provider provider, VolleyError error);
        void onProviderRemoved(Provider provider);
    }

    public boolean isDone() {
        return isDone;
    }

    protected void handleError(VolleyError error) {
        isDone = true;
        //Toast.makeText(context, context.getString(R.string.failed_to_fetch_resource), Toast.LENGTH_LONG).show();
        Log.e(getClass().getName(), context.getString(R.string.failed_to_fetch_resource) + error);
        if (callback != null)
            callback.onProviderDataError(this, error);
    }

    protected void handleData(JSONObject response) {
    }
    protected void handleData(JSONArray response) {
    }
    protected void handleData(String response) {
    }

    protected void getString(String url) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleData(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                    }
                });

        queue.add(jsonObjectRequest);
    }

    protected void getStandardJsonObject(String url) {
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handleData(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                    }
                });

        queue.add(jsonObjectRequest);
    }

    protected void getStandardJsonArray(String url) {
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        handleData(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                    }
                });

        queue.add(jsonObjectRequest);
    }
}
