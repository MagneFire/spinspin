package org.magnefire.spinspin;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ListAdapter extends ArrayAdapter<Reward> {
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_HEADER = 1;

    private Context mContext;

    ListAdapter(Context context, List<Reward> rewards) {
        super(context, 0, rewards);
        this.mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof MainActivity.HeaderReward) {
            return VIEW_TYPE_HEADER;
        }
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int type = getItemViewType(position);
        View v = convertView;
        Reward p = getItem(position);

        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(
                    type == VIEW_TYPE_ITEM ? R.layout.activity_listitem : R.layout.activity_listitemheader, parent, false);
        }


        TextView tt3 = v.findViewById(R.id.description);
        TextView dv = v.findViewById(R.id.dateView);
        ImageView iv = v.findViewById(R.id.imageView);
        if (tt3 != null) {
            if (p instanceof MainActivity.HeaderReward) {
                v.setOnClickListener(null);
                v.setOnLongClickListener(null);
                v.setLongClickable(false);
                tt3.setText(((MainActivity.HeaderReward) p).headerText);
            } else {
                String title = "";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d-M-yyyy HH:mm", Locale.US);
                String dateString = simpleDateFormat.format(p.date);
                Reward.RewardType rewardType = p.itemType;
                if (rewardType == Reward.RewardType.REWARD_TYPE_UNKNOWN) {
                    iv.setImageResource(R.mipmap.surprise);
                    title = mContext.getString(R.string.reward_surprise);
                } else if ((rewardType == Reward.RewardType.REWARD_TYPE_COINS) || (rewardType == Reward.RewardType.REWARD_TYPE_SPINS_AND_COINS)) {
                    iv.setImageResource(R.mipmap.coin);
                    if (p.nrOfCoins > 0) {
                        if (p.multiplierCoins == 1000) {
                            title = mContext.getString(R.string.reward_coins_k, p.nrOfCoins);
                        } else if (p.multiplierCoins == 1000000) {
                            title = mContext.getString(R.string.reward_coins_m, p.nrOfCoins);
                        } else if (p.multiplierCoins < 1000000) {
                            title = mContext.getString(R.string.reward_coins_m_f, (p.nrOfCoins * p.multiplierCoins)/1000000.0);
                        } else if (p.multiplierCoins == 1000000000) {
                            title = mContext.getString(R.string.reward_coins_b, p.nrOfCoins);
                        } else if (p.multiplierCoins < 1000000000) {
                            title = mContext.getString(R.string.reward_coins_b_f, (p.nrOfCoins * p.multiplierCoins)/1000000000.0);
                        } else {
                            title = mContext.getString(R.string.reward_coins, p.nrOfCoins);
                        }
                    }
                } else if (rewardType == Reward.RewardType.REWARD_TYPE_SPINS) {
                    iv.setImageResource(R.mipmap.spin);
                    if (p.nrOfSpins > 0) {
                        title = mContext.getString(R.string.reward_spins, p.nrOfSpins);
                    }
                }
                if (rewardType == Reward.RewardType.REWARD_TYPE_SPINS_AND_COINS) {
                    iv.setImageResource(R.mipmap.spin_coin);
                    if (p.nrOfCoins > 0 && p.nrOfSpins > 0) {
                        if (p.multiplierCoins == 1000) {
                            title = mContext.getString(R.string.reward_spins_and_coins_k, p.nrOfSpins, p.nrOfCoins);
                        }else if (p.multiplierCoins == 1000000) {
                            title = mContext.getString(R.string.reward_spins_and_coins_m, p.nrOfSpins, p.nrOfCoins);
                        } else if (p.multiplierCoins == 1000000000) {
                            title = mContext.getString(R.string.reward_spins_and_coins_b, p.nrOfSpins, p.nrOfCoins);
                        } else {
                            title = mContext.getString(R.string.reward_spins_and_coins, p.nrOfSpins, p.nrOfCoins);
                        }
                    }
                }

                String htmlString = "<big><big><strong>" + title + "</strong></big></big>";
                String htmlDate = "<small><strong>" + dateString + "</strong></small>";

                if (p.used) {
                    ColorMatrix matrix = new ColorMatrix();
                    matrix.setSaturation(0);  //0 means grayscale
                    ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
                    iv.setColorFilter(cf);
                    iv.setImageAlpha(128);   // 128 = 0.5
                    htmlString = "<strike>" + htmlString + "</strike>";
                    htmlDate = "<strike>" + htmlDate + "</strike>";
                } else {
                    iv.setColorFilter(null);
                    iv.setImageAlpha(255);
                }
                dv.setText(Html.fromHtml(htmlDate));
                tt3.setText(Html.fromHtml(htmlString));
            }
        }
        return v;
    }
}