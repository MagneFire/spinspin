package org.magnefire.spinspin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.VolleyError;

import org.magnefire.spinspin.Providers.Haktuts;
import org.magnefire.spinspin.Providers.egz;
import org.magnefire.spinspin.Providers.hwh;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, Provider.OnProviderReady, DownloadManager.Complete, ScriptParser.ScriptReady {
    SwipeRefreshLayout pullToRefresh;

    private ProviderManager providerManager;

    private SharedPreferences pref;

    private ListAdapter customAdapter;
    private List<Reward> rewards;

    private Context context;
    public static final String URL_MESSAGE = "org.magnefire.spinspin.URL";
    public static final String POSITION_MESSAGE = "org.magnefire.spinspin.POSITION";
    public static final String SCRIPT_DATE = "org.magnefire.spinspin.SCRIPT_DATE";
    public static final String SCRIPT_URL = "http://spinspin.nl/";
    public static final String SETTING_DARK_MODE = "SETTING_DARK_MODE";

    public static final String PROVIDERS_URL = "https://gitlab.com/MagneFire/spinspin/raw/master/script.js";

    public static final String SCRIPT_NAME = "providers.js";

    private CountDownTimer fetchTimout;

    private Calendar mCurrentDate;

    // Date format used to keep track fo when the script was last updated.
    private SimpleDateFormat mScriptDateFormat;

    ScriptParser mScriptParser;

    private static DownloadManager mDownloadManager = new DownloadManager();

    private void updateCurrentTime() {
        // Update the current date.
        mCurrentDate = Calendar.getInstance(Locale.ENGLISH);
    }

    private void fetchLists() {
        pullToRefresh.setRefreshing(true);
        // Set current time minus 10 days.
        updateCurrentTime();
        // Initiate a reward list fetch.
        providerManager.requestRewards();
        // Restart timeout timer.
        fetchTimout.cancel();
        fetchTimout.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        boolean darkMode = pref.getBoolean(SETTING_DARK_MODE, false);
        if (darkMode) {
            setTheme(R.style.AppTheme_Material_Black);
        } else {
            setTheme(R.style.AppTheme_Material);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set current time minus 10 days.
        updateCurrentTime();
        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!providerManager.isEmpty())
                    fetchLists();
                else
                    pullToRefresh.setRefreshing(false);
            }
        });

        mDownloadManager.setCallback(this);

        providerManager = ProviderManager.getInstance();
        providerManager.setOnProviderReady(this);
        providerManager.addProvider(new Haktuts(this));
        providerManager.addProvider(new hwh(this));
        providerManager.addProvider(new egz(this));

        fetchTimout = new CountDownTimer(30000, 2000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (!providerManager.allFetched()) {
                    // Providers are bound to go down from time to time, ignore missing data.
                    pullToRefresh.setRefreshing(false);
                }
            }
        };


        context = this;

        rewards = new ArrayList<>();
        Reward hReward = new HeaderReward(getString(R.string.reward_header_unused).toUpperCase(), false);
        rewards.add(hReward);
        hReward = new HeaderReward(getString(R.string.reward_header_used).toUpperCase(), true);
        rewards.add(hReward);

        ListView listview = findViewById(R.id.listView1);
        listview.setOnItemClickListener(this);
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                final Reward reward = rewards.get(pos);

                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle(getString(R.string.reward_used_title));
                if (reward.used)
                    dialog.setMessage(getString(R.string.reward_used_msg_unused));
                else
                    dialog.setMessage(getString(R.string.reward_used_msg_used));
                dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Mark reward as used/unused.
                        reward.used = !reward.used;

                        // Save data.
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean(Provider.PROVIDER_ITEM_USED + reward.url, reward.used);
                        editor.apply();

                        // Sort list again...
                        sortItems();
                    }
                });
                dialog.setNegativeButton(android.R.string.no, null);
                dialog.show();
                return true;
            }
        });
        // get data from the table by the ListAdapter
        customAdapter = new ListAdapter(this, rewards);
        listview.setAdapter(customAdapter);

        // Get script from URL.
        Uri data = this.getIntent().getData();
        if (data != null && data.isHierarchical()) {
            String uri = this.getIntent().getDataString();
            if (uri != null) {
                try {
                    uri = URLDecoder.decode(uri, "UTF-8");
                    if (uri.contains(SCRIPT_URL)) {
                        uri = uri.substring(SCRIPT_URL.length());
                        if (uri.contains("/")) {
                            String type = uri.substring(0, uri.indexOf('/'));
                            if (type.equals("settings")) {
                                String settings = uri.substring(uri.indexOf('/')+1);
                                SharedPreferences.Editor editor = pref.edit();
                                // Update app theme.
                                if (settings.contains(SETTING_DARK_MODE)) {
                                    editor.putBoolean(SETTING_DARK_MODE, true);
                                } else {
                                    editor.putBoolean(SETTING_DARK_MODE, false);
                                }
                                editor.apply();
                                // Restart app to apply theme.
                                finishAffinity();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        mScriptParser = new ScriptParser(this, this);
        mScriptParser.setCallback(this);
        mScriptDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);

        String scriptDate = pref.getString(SCRIPT_DATE, null);
        if (scriptDate != null) {
            try {
                Date date = mScriptDateFormat.parse(scriptDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                // Add offset to check if current script is older than a day.
                calendar.add(Calendar.DATE, 1);
                if (mCurrentDate.compareTo(calendar) < 0) {
                    // Script is not older than a day => Parse script.
                    mScriptParser.parseFromFile(SCRIPT_NAME);
                } // Else => onResume will update script.
            } catch (Exception e) {
                // Error parsing date, download providers just in case.
                downloadProvider();
                e.printStackTrace();
            }
        } else {
            // No script loaded, load provider, while ignoring possible disabled ones by script.
            // A app reload will fix this.
            onJsLoaded();
            downloadProvider();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // When the app is up for a day, but in the background we need to check upon onResume.
        // Only update the provider when older than a day.
        String scriptDate = pref.getString(SCRIPT_DATE, null);
        if (scriptDate != null) {
            try {
                Date date = mScriptDateFormat.parse(scriptDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                // Add offset to check if current script is older than a day.
                calendar.add(Calendar.DATE, 1);
                if (mCurrentDate.compareTo(calendar) >= 0) {
                    // It appears that the current script is older than a day. Update providers.
                    downloadProvider();
                } // Else => onCreate has already loaded the script.
            } catch (Exception e) {
                // Same error will occur as in onCreate => download new provider.
            }
        }
    }

    private void downloadProvider() {
        try{
            mDownloadManager.execute(new DownloadManager.File(PROVIDERS_URL, openFileOutput(SCRIPT_NAME, Context.MODE_PRIVATE)));
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
    }

    public void OnDownloadComplete(DownloadManager.File file, boolean success) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SCRIPT_DATE, mScriptDateFormat.format(mCurrentDate.getTime()));
        editor.apply();
        // Parse script.
        mScriptParser.parseFromFile(SCRIPT_NAME);
    }

    public void onJsLoaded() {
        // Fetch data from providers.
        if (!providerManager.isEmpty())
            fetchLists();
    }

    private boolean addItem(Reward reward) {
        Reward found = null;
        for (Reward it : rewards) {
            if (reward.url.equals(it.url)) {
                found = it;
            }
        }
        if (found == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(reward.date);
            // Add 10 days, assume that links older than 10 days are invalid.
            calendar.add(Calendar.DATE, 10);

            if (calendar.compareTo(mCurrentDate) >= 0) {
                rewards.add(reward);
                return true;
            } else {
                // Remove old item from shared preferences.
                SharedPreferences.Editor editor = pref.edit();
                editor.remove(Provider.PROVIDER_ITEM_USED + reward.url);
                editor.apply();
            }
            // Link is expired.
            return false;
        } else {
            // Update current reward if Reward quality is better.
            if (reward.predictionQuality > found.predictionQuality) {
                found.itemType = reward.itemType;
                found.multiplierCoins = reward.multiplierCoins;
                found.nrOfCoins = reward.nrOfCoins;
                found.nrOfSpins = reward.nrOfSpins;
                found.predictionQuality = reward.predictionQuality;
                found.date = reward.date;
                // Data changed, return true to indicate that an reward has changed.
                return true;
            }
            // Update the date of found reward if needed.
            Date date = found.date;
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            // Update the date when the year is equal to 1970(epoch) or when time is not set.
            if (calendar.get(Calendar.YEAR) == 1970 || ((calendar.get(Calendar.HOUR_OF_DAY) == 0) && (calendar.get(Calendar.MINUTE) == 0))) {
                date.setTime(reward.date.getTime());
                // Data changed, return true to indicate that an reward has changed.
                return true;
            }
        }
        return false;
    }

    private void sortItems() {
        Collections.sort(rewards, new Comparator<Reward>() {
            @Override
            public int compare(Reward o1, Reward o2) {
                // First sort by reward used/not used, then by date.
                int usedComp = Boolean.compare(o1.used, o2.used);
                if (usedComp != 0) {
                    return usedComp;
                }
                if (o1 instanceof HeaderReward) {
                    if (o1.used)
                        return -1;
                    else
                        return 1;
                }
                if (o2 instanceof HeaderReward) {
                    if (o2.used)
                        return 1;
                    else
                        return 1;
                }

                return o2.date.compareTo(o1.date);
            }
        });
        customAdapter.notifyDataSetChanged();
    }

    public void onProviderDataReady(Provider provider) {
        if (providerManager.allFetched()) {
            fetchTimout.cancel();
            pullToRefresh.setRefreshing(false);
        }
        // A provider was removed. updating the pull to refresh is enough.
        if (provider == null) return;
        Log.i(getClass().getName(), "DATA READY " + provider + " Items: " + provider.rewards.size());
        boolean arrayChanged = false;
        for (Reward reward : provider.rewards) {
            reward.used = pref.getBoolean(Provider.PROVIDER_ITEM_USED + reward.url, false);
            boolean added = addItem(reward);
            if (added)
                arrayChanged = true;
        }
        // Only sort & signal data changed when data was changed.
        if (arrayChanged) {
            sortItems();
        }
    }

    public void onProviderRemoved(Provider provider) {
        // Update refresh state.
        onProviderDataReady(null);
    }

    public void onProviderDataError(Provider provider, VolleyError error) {
        // Error occurred, calling the data ready function disables the timer and refresher if needed.
        onProviderDataReady(provider);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                // Sort rewards and notify ListView that the data changed.
                // This moves the current item down to the rest of the used rewards.
                sortItems();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                int position = data.getIntExtra(POSITION_MESSAGE, -1);

                Reward reward = rewards.get(position);
                // Mark reward as opened.
                reward.used = false;

                // Save data.
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Provider.PROVIDER_ITEM_USED + reward.url, reward.used);
                editor.apply();
            }
        }
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Reward reward = rewards.get(position);
        if (reward.url == null) return;
        // The reward is here marked as used. Because the UrlLoader can open the associated app.
        // This may kill this application, marking the reward as used here still saves the used state.
        // If for some reason the reward was not used(i.e. associated app not found/no internet connection)
        // The UrlLoader will return with a RESULT_CANCELLED state, this will undo the used mark here.
        // Sorting here is not useful and only reduces performance as a new intent is opened here.
        // Instead this is done once a reward is confirmed as used.

        // Mark reward as opened.
        reward.used = true;

        // Save data.
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Provider.PROVIDER_ITEM_USED + reward.url, reward.used);
        editor.apply();

        // Open link!
        Intent intent = new Intent(context, UrlLoader.class);
        intent.putExtra(URL_MESSAGE, reward.url);
        intent.putExtra(POSITION_MESSAGE, position);

        startActivityForResult(intent, 1);
    }

    public class HeaderReward extends Reward {
        String headerText;

        HeaderReward(String text, boolean used) {
            super(null);
            this.headerText = text;
            this.used = used;
        }
    }
}
