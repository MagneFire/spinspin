package org.magnefire.spinspin;

import java.util.ArrayList;

public class ProviderManager {
    private ArrayList<Provider> mProviders;
    private Provider.OnProviderReady mCallback = null;
    private static ProviderManager manager = null;

    public static ProviderManager getInstance() {
        if (manager == null) {
            manager = new ProviderManager();
        }
        return manager;
    }

    private ProviderManager(){
        mProviders = new ArrayList<>();
    }

    public void setOnProviderReady(Provider.OnProviderReady callback) {
        mCallback = callback;
        // Update all providers.
        for (Provider provider : mProviders) {
            provider.setOnProviderReady(mCallback);
        }
    }
    public void addProvider(Provider provider) {
        mProviders.add(provider);
        // Set callback. If null than this will be fixed when the setOnProviderReady function is called.
        provider.setOnProviderReady(mCallback);
    }

    public void removeProvider(Provider provider) {
        if (provider == null) return;
        mProviders.remove(provider);
        // Remove callback, used to indicate that a provider is removed.
        provider.setOnProviderReady(null);
    }

    public void removeProvider(String provider) {
        if (provider == null) return;
        for (Provider p : mProviders) {
            if (provider.equals(p.mProviderName)) {
                removeProvider(p);
                return;
            }
        }

    }

    public void requestRewards() {
        // Request rewards lists from all providers.
        for (Provider provider : mProviders) {
            provider.requestList();
        }
    }

    public boolean allFetched() {
        boolean isDone = true;
        for (Provider provider1 : mProviders) {
            isDone = isDone && provider1.isDone();
        }
        return isDone;
    }
    public boolean isEmpty() {
        return mProviders.size() == 0;
    }
}
