# SpinSpin

Simple Android application that provides links to free spins & coins.
It combines multiple sources of links to always provide a sufficient amount of links.
Also links will be opened in the background, without the need for an external browser.